# Manual Calendari Nextcloud per iOS

1. Entrar a Seguretat -> "Crea una nova contrasenya d'aplicació" (https://nextcloud.DOMINI/settings/user/security)

![](img/calendari-nextcloud-ios/manual_ios1.png)



**Es crea una contrasenya molt llarga i l'has de guardar per fer-la servir després.**

2. Desde l'iOS, entrar al navegador amb l'adreça (https://nextcloud.DOMINI/remote.php/dav/principals/users/NOM_USUARI/), on posa NOM_USUARI és el vostre usuari de nextcloud i et surt una pàgina on has de posar el teu nom d'usuari i contrasenya que has generat abans a la web.

Si s'ha fet bé et redirigeix a una pàgina com aquesta:

![](img/calendari-nextcloud-ios/manual_ios2.png)



3. Ajustos del dispositiu iOS -> Calendari -> Comptes -> Afegir compte -> Altre -> Afegir compte CalDAV

![](img/calendari-nextcloud-ios/manual_ios3.png)



- Server: Va l'adreça amb el teu nom d'usuari https://nextcloud.DOMINI/remote.php/dav/principals/users/NOM_USUARI/
- Username: el teu nom d'usuari
- Password: la contrasenya generada d'abans
- Description: el nom que li vols donar (per defecte posarà el del server)

Llavors fas clic a "Advanced Settings" i habilites el botó de "Use SSL" (el reste de dades s'omplen soles, sinò poses 443 al port i el mateix URL d'abans)

![](img/calendari-nextcloud-ios/manual_ios4.png)



I en principi ja està, es sincronitza directament. Si no s'afegeix al teu calendari directament:

Calendari (Aplicació) -> i afegeix la compta del nextcloud que acaves de configurar.
